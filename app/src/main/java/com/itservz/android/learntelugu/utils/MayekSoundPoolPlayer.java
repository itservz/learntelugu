package com.itservz.android.learntelugu.utils;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;


import com.itservz.android.learntelugu.R;

import java.util.HashMap;

/**
 * Created by raju.athokpam on 24-08-2016.
 */
public class MayekSoundPoolPlayer {
    private SoundPool mShortPlayer = null;
    private HashMap mSounds = new HashMap();

    public MayekSoundPoolPlayer(Context pContext) {
        this.mShortPlayer = new SoundPool(4, AudioManager.STREAM_MUSIC, 0);
        mSounds.put(R.drawable.a, this.mShortPlayer.load(pContext, R.raw.click, 1));
        mSounds.put(R.drawable.aa, this.mShortPlayer.load(pContext, R.raw.click, 1));

    }

    public void playShortResource(int piResource) {
        int iSoundId = (Integer) mSounds.get(piResource);
        this.mShortPlayer.play(iSoundId, 0.99f, 0.99f, 0, 0, 1);
    }

    public void release() {
        this.mShortPlayer.release();
        this.mShortPlayer = null;
    }
}

package com.itservz.android.learntelugu.utils;


import com.itservz.android.learntelugu.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Raju on 8/25/2016.
 */
public class Mayeks {
    private List<MayekCard> cards = null;
    private static Mayeks instance = null;
    private Mayeks(){
        cards = new ArrayList<>();
        cards.add(new MayekCard("A", R.drawable.a, R.drawable.a, R.raw.click));
        cards.add(new MayekCard("AA", R.drawable.aa, R.drawable.aa, R.raw.click));

    }

    public static Mayeks getInstance(){
        if(instance == null ){
            instance = new Mayeks();
        }
        return instance;
    }

    public List<MayekCard> getCards(){
        return cards;
    }


}

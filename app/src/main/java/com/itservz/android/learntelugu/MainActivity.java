package com.itservz.android.learntelugu;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.BounceInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.itservz.android.learntelugu.utils.BackgroundMusicFlag;
import com.itservz.android.learntelugu.utils.SoundPoolPlayer;
import com.itservz.android.learntelugu.vowels.MayekActivity;


public class MainActivity extends BaseActivity  {

    private SoundPoolPlayer soundPoolPlayer;
    private ImageView noticeBoard;
    private ImageView mayekBoard;
    private ImageView cardBoard;
    private ImageView soundView;
    private ImageView noticeView;
    private ImageView masingView;
    private boolean doubleBackToExitPressedOnce;
    private Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        startService(backgroundMusicService);
        BackgroundMusicFlag.getInstance().setSoundOnOff(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        animate();
        soundPoolPlayer = new SoundPoolPlayer(getApplicationContext());
        if(!wentToAnotherActivity && BackgroundMusicFlag.getInstance().isSoundOnOff()){
            startService(backgroundMusicService);
        }
        wentToAnotherActivity = false;
    }

    @Override
    protected void onPause(){
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        soundPoolPlayer.release();
        if(!wentToAnotherActivity){
            stopService(backgroundMusicService);
        }
    }

    @Override
    protected void onDestroy(){
        stopService(backgroundMusicService);
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

    public void click(View view) {
        if (view.getId() == R.id.mayekBoardButton ) {
            //soundPoolPlayer.playShortResource(R.raw.whoa);
            wentToAnotherActivity = true;
            Intent intent = new Intent(this, MayekActivity.class);
            startActivity(intent);
        } else if (view.getId() == R.id.noticeboard) {
            //soundPoolPlayer.playShortResource(R.raw.whoa);
            animateNoticeBoard();
        } else if (view.getId() == R.id.info) {
            dialog = new Dialog(this, R.style.full_screen_dialog);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.floating_setting);
            dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            dialog.getWindow().setFormat(PixelFormat.TRANSLUCENT);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

            RadioGroup radio = (RadioGroup) dialog.findViewById(R.id.radioSound);
            if (BackgroundMusicFlag.getInstance().isSoundOnOff()) {
                radio.check(R.id.on);
            } else {
                radio.check(R.id.off);
            }
            dialog.show();

            radio.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {

                    RadioButton radioButton = (RadioButton) radioGroup.findViewById(checkedId);
                    int index = radioGroup.indexOfChild(radioButton);
                    switch (index) {
                        case 0:
                            startService(backgroundMusicService);
                            BackgroundMusicFlag.getInstance().setSoundOnOff(true);
                            radioGroup.check(R.id.on);
                            break;
                        case 1:
                            stopService(backgroundMusicService);
                            BackgroundMusicFlag.getInstance().setSoundOnOff(false);
                            radioGroup.check(R.id.off);
                            break;
                    }
                }
            });

            Button mayekplay = (Button) dialog.findViewById(R.id.mayekplay);
            mayekplay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final String appPackageName = "com.itservz.android.mayekplay";
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                    }
                    dialog.dismiss();
                }
            });

            ImageView rate = (ImageView) dialog.findViewById(R.id.rate);
            rate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.itservz.android.mayekid")));
                    dialog.dismiss();
                }
            });

            ImageView back = (ImageView) dialog.findViewById(R.id.back);
            back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });

        }
    }

    private void clearAnimations() {
        noticeBoard.clearAnimation();
        mayekBoard.clearAnimation();
        cardBoard.clearAnimation();
        soundView.clearAnimation();
        noticeView.clearAnimation();
    }

    private void animate() {
        mayekBoard = (ImageView) findViewById(R.id.mayekBoardButton);
        cardBoard = (ImageView) findViewById(R.id.cartoonBoardButton);
        soundView = (ImageView) findViewById(R.id.info);
        noticeView = (ImageView) findViewById(R.id.noticeboard);
        masingView = (ImageView) findViewById(R.id.masing_board);

        Animation animation = AnimationUtils.loadAnimation(this, R.anim.scale_animation01);
        Animation slowAnimation = AnimationUtils.loadAnimation(this, R.anim.scale_animation01);
        slowAnimation.setDuration(1800);

        mayekBoard.startAnimation(animation);
        masingView.startAnimation(animation);

        cardBoard.startAnimation(slowAnimation);
        soundView.startAnimation(slowAnimation);
        noticeView.startAnimation(slowAnimation);
    }

    private void animateNoticeBoard() {
        noticeBoard = (ImageView) findViewById(R.id.noticeboard);

        int originalPos[] = new int[2];
        noticeBoard.getLocationOnScreen(originalPos);

        AnimationSet animSet = new AnimationSet(true);
        animSet.setFillAfter(true);
        animSet.setDuration(1000);
        animSet.setInterpolator(new BounceInterpolator());

        TranslateAnimation translate = new TranslateAnimation(originalPos[0], originalPos[0], -noticeBoard.getMeasuredHeight(), 0);
        animSet.addAnimation(translate);

        noticeBoard.startAnimation(animSet);
    }
}

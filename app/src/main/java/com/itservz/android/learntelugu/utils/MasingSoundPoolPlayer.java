package com.itservz.android.learntelugu.utils;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;


import com.itservz.android.learntelugu.R;

import java.util.HashMap;

/**
 * Created by raju.athokpam on 24-08-2016.
 */
public class MasingSoundPoolPlayer {
    private SoundPool mShortPlayer = null;
    private HashMap mSounds = new HashMap();

    public MasingSoundPoolPlayer(Context pContext) {
        this.mShortPlayer = new SoundPool(4, AudioManager.STREAM_MUSIC, 0);

    }

    public void playShortResource(int piResource) {
        int iSoundId = (Integer) mSounds.get(piResource);
        this.mShortPlayer.play(iSoundId, 0.99f, 0.99f, 0, 0, 1);
    }

    public void release() {
        this.mShortPlayer.release();
        this.mShortPlayer = null;
    }
}
